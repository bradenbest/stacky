#ifndef CRYPT_H
#define CRYPT_H

#include <stdint.h>
#include <stddef.h>

#define base64_encode(buffer, text, textlen) \
    (base64_encode)((buffer), (char const *)(text), (textlen))

#define base64_decode(buffer, text, textlen) \
    (base64_decode)((buffer), (char const *)(text), (textlen))

#define crypt_encode(buffer, text) \
    (crypt_encode)((buffer), (char const *)(text))

char *  (crypt_encode)         (char *buffer, char const *text);
char *  (crypt_decode)         (char *buffer, char *text);
char *  (crypt_fucko_reverse)  (char *buffer);
char *  (crypt_rot13)          (char *text);
char *  (base64_encode)        (char *buffer, char const *text, size_t textlen);
char *  (base64_decode)        (char *buffer, char const *text, size_t textlen);

#endif
