#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "crypt.h"

#define MIN(a, b) \
    ( (a) < (b) ? (a) : (b) )

#define base64_encode_chunk(buffer, chunk, len) \
    (base64_encode_chunk)((buffer), (char const *)(chunk), len)

#define base64_decode_chunk(buffer, chunk) \
    (base64_decode_chunk)((buffer), (char const *)(chunk))

static char *  (base64_encode_chunk)  (char *buffer, char const *chunk, size_t len);
static char *  (base64_decode_chunk)  (char *buffer, char const *chunk);
static char *  (crypt_reverse_chunk)  (char *buffer);
static char    (rot13_byte)           (char byte);

static char const *base64_charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

static char *
(base64_encode_chunk)(char *buffer, char const *chunk, size_t len)
{
    static uint8_t idxes[4];

    idxes[0] = (chunk[0] >> 2) & 0x3f;
    idxes[1] = ((chunk[0] << 4) & 0x30) | ((chunk[1] >> 4) & 0x0f);
    idxes[2] = ((chunk[1] << 2) & 0x3c) | ((chunk[2] >> 6) & 0x03);
    idxes[3] = chunk[2] & 0x3f;

    buffer[0] = base64_charset[idxes[0]];
    buffer[1] = base64_charset[idxes[1]];
    buffer[2] = base64_charset[idxes[2]];
    buffer[3] = base64_charset[idxes[3]];

    switch(len){
        case 2:
            buffer[3] = '=';
            break;
        case 1:
            buffer[2] = '=';
            buffer[3] = '=';
            break;
        default:
            break;
    }

    return buffer + 4;
}

static char *
(base64_decode_chunk)(char *buffer, char const *chunk)
{
    static uint8_t idxes[4];
    size_t len = 3;

    idxes[0] = strchr(base64_charset, chunk[0]) - base64_charset;
    idxes[1] = strchr(base64_charset, chunk[1]) - base64_charset;
    idxes[2] = strchr(base64_charset, chunk[2]) - base64_charset;
    idxes[3] = strchr(base64_charset, chunk[3]) - base64_charset;

    if(idxes[2] == 64)
        len = 1;
    else if(idxes[3] == 64)
        len = 2;

    buffer[0] = (idxes[0] << 2) | ((idxes[1] >> 4) & 0x03);
    buffer[1] = ((idxes[1] << 4) & 0xf0) | ((idxes[2] >> 2) & 0x0f);
    buffer[2] = ((idxes[2] << 6) & 0xc0) | (idxes[3] & 0x3f);
    buffer[len] = 0;

    return buffer + len;
}

static char *
(crypt_reverse_chunk)(char *buffer)
{
    static char temp[2];

    temp[0] = buffer[0];
    temp[1] = buffer[1];
    buffer[0] = buffer[3];
    buffer[1] = buffer[2];
    buffer[3] = temp[0];
    buffer[2] = temp[1];

    return buffer + 4;
}

static char
(rot13_byte)(char byte)
{
    char offset = 0;

    if(byte >= 'A' && byte <= 'Z')
        offset = 'A';

    if(byte >= 'a' && byte <= 'z')
        offset = 'a';

    if(offset != 0)
        return (byte + 13 - offset) % 26 + offset;

    return byte;
}

char *
(crypt_fucko_reverse)(char *buffer)
{
    size_t buflen = strlen(buffer);
    char *bufp = buffer;

    for(size_t i = 0; i < buflen; i += 4)
        bufp = crypt_reverse_chunk(bufp);

    return buffer;
}

char *
(base64_encode)(char *buffer, char const *text, size_t textlen)
{
    char *bufp = buffer;

    if(buffer == NULL)
        return NULL;

    for(size_t i = 0; i < textlen; i += 3)
        bufp = base64_encode_chunk(bufp, text + i, MIN(textlen - i, 3));

    *bufp = 0;
    return buffer;
}

/* Note: Don't ever use null-terminated strings in a full base64 decoder.
 * This decoder does NOT expect binary. It was written for the esolang.
 * -Braden
 */
char *
(base64_decode)(char *buffer, char const *text, size_t textlen)
{
    char *bufp = buffer;

    if(buffer == NULL)
        return NULL;

    for(size_t i = 0; i < textlen; i += 4)
        bufp = base64_decode_chunk(bufp, text + i);

    *bufp = 0;
    return buffer;
}

char *
(crypt_rot13)(char *text)
{
    for(char *txp = text; *txp; ++txp)
        *txp = rot13_byte(*txp);

    return text;
}

char *
(crypt_encode)(char *buffer, char const *text)
{
    base64_encode(buffer, text, strlen(text));
    crypt_fucko_reverse(buffer);
    return crypt_rot13(buffer);
}

char *
(crypt_decode)(char *buffer, char *text)
{
    crypt_rot13(text);
    crypt_fucko_reverse(text);
    return base64_decode(buffer, text, strlen(text));
}
