/* Stacky "compiler" - frontend for crypt_encode */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "file.h"
#include "crypt.h"
#include "util.h"

#define GET_BUFSZ_BASE64_ENCODE(len) \
    ( ((2 + (len)) / 3) * 4 + 1 )

static void
usage(void)
{
    puts("sc - minimizing stacky compiler");
    puts("usage: sc <file>");
    exit(1);
}

int
main(int argc, char **argv)
{
    char *filename;
    FILE *fp;
    char *file;
    size_t bufsize;
    char *buffer;

    if(!safe_assign(filename = argv[1]))
        usage();

    if(!safe_assign(fp = fopen(filename, "r")))
        return 1;

    if(!safe_assign(file = file_minify(fp)))
        return 1;

    bufsize = GET_BUFSZ_BASE64_ENCODE(strlen(file));

    if(!safe_assign(buffer = malloc(bufsize)))
        return 1;

    puts(crypt_encode(buffer, file));
    free(buffer);
    free(file);
    fclose(fp);
    return 0;
}
