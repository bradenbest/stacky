#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "file.h"
#include "stacky.h"

static size_t  (get_filesize)  (FILE *);
static char *  (read_instr)    (char *buffer, int instr, FILE *fp);
static char *  (read_string)   (char *buffer, FILE *fp);
static char *  (read_number)   (char *buffer, int digit0, FILE *fp);

static size_t
(get_filesize)(FILE *fp)
{
    long pos;

    fseek(fp, 0, SEEK_END);
    pos = ftell(fp);
    rewind(fp);

    return pos;
}

static char *
(read_string)(char *buffer, FILE *fp)
{
    int ch;

    *buffer++ = '\'';

    while((ch = fgetc(fp)) != '\'')
        *buffer++ = ch;

    *buffer++ = ch;
    return buffer;
}

static char *
(read_number)(char *buffer, int digit0, FILE *fp)
{
    int ch;

    *buffer++ = digit0;

    while((ch = fgetc(fp)) >= '0' && ch <= '9')
        *buffer++ = ch;

    ungetc(ch, fp);
    return buffer;
}

static char *
(read_instr)(char *buffer, int instr, FILE *fp)
{
    int ch;
    *buffer++ = instr;

    if(strchr(stacky_arg_ops, instr) == NULL)
        return buffer;

    ch = fgetc(fp);

    if(instr == 'p'){
        if(ch == '\'')
            return read_string(buffer, fp);

        return read_number(buffer, ch, fp);
    }

    /* ^ and # jumps */
    return read_number(buffer, ch, fp);
}

char *
(file_minify)(FILE *fp)
{
    char *out = malloc(get_filesize(fp) + 1);
    char *outp = out;
    int ch;

    while((ch = fgetc(fp)) != EOF)
        if(strchr(stacky_ops, ch) != NULL)
            outp = read_instr(outp, ch, fp);

    *outp = '\0';
    return out;
}
