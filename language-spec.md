# Specification

A stacky ("stak-ee") program is a big long base64 string, but each group of 4 characters is reversed, so you need to write a funky
base64 decoder in order to parse it.

A stackyzi ("stak-ee-zee") program is written with the following operators:

# Operations

    pv   push v to stack. v can be a number or a string delimited by surrounding '.
             values are modulo 256 and strings are pushed character-by-character
    i    push character from stdin to stack
    o    pop from stack and print as character
    .    pop from stack and print as characters until a zero is reached
             if empty stack is popped, interpreter will quit with error message "IM DED XP"
             this applies to all instructions that pop from the stack
    n    pop from stack and print as number. In hexadecimal.
    s    pop from stack and store in register
    l    push value in register to stack
    d    duplicate top of stack
             empty stack = crash
    w    swap top of stack with value under it
             stack length < 2 = crash
    +    pop two values from stack and add, push result to stack
    -    pop two values from stack and subtract first popped value from second popped value
             same semantics as +
    ^v   pop from stack and jump forward v instructions if value is zero, otherwise move to next instruction
             if jumping out of bounds, interpreter will quit with error message "IM LOST D:"
    #v   jump backward v instructions unconditionally
             same semantics as ^
    e    end the program (required)

# Examples

    e                          => no op
    .                          => crashes ("ENDLESS PROGRAM :O")
    p10.e                      => crashes ("IM DED XP")
    p1+e                       => crashes ("IM DED XP")
    p10e                       => pushes a newline to the stack
    p'olleH'e                  => pushes "olleH" to the stack
    p'olleH'oooooe             => pushes "olleH" to the stack and prints top 5 characters ("Hello")
    p0p'olleH'.e               => pushes null-terminated "olleH" to the stack, prints "Hello"
    p1^7idop33-#6e             => echoes user input until they enter `!`
    p0^2e                      => crashes ("IM LOST D:")
    p65oe                      => prints "A"
    p'iH'.e                    => prints "Hi" then crashes with "IM DED XP"
    p0p'iH'.e                  => prints "Hi"
    p0p10p55+.e                => prints "A"
    p65ne                      => prints "41"
    p10p2-ne                   => prints "08"
    p10dp6s^9p1-p6l+sd#8lp5+oe => prints "A" by multiplying 10x6 and adding 5
    p65o#2e                    => prints "A" infinitely
    islp'0'-^5llo#4loe         => truth machine
    idp'0'-^5ddo#4oe           => truth machine (registerless)

# Other notes

characters that aren't operators or arguments to an operator are ignored

Program is required to end with `e`

comments go after the `e`

The stack is 4 KiB

every item on the stack is 8-bit unsigned, 255 + 1 overflows to 0.

There is exactly one "register" and its purpose is to act as a temp space to hold a value from the stack.

The register is not cleared by the `l` operator, so `slllll` is a valid way of duping the top stack value 5 times

Register value is 0 by default.

Stack values are not cleared by popping, but since there is no way to manually move the stack pointer, that doesn't
matter.

There is no escaping in strings. If you want a ' in your string, push its ascii code to the stack.

# Writing a Hello World program in Stackyzi

    p0 p10 p'!dlroW ,olleH' .e

# Writing a Hello World program in Stacky

Convert the Stackyzi program to base64

    cDAgcDEwIHAnIWRscm9XICxvbGxlSCcgLmUK

break it into 4-character chunks

    cDAg
    cDEw
    ...
    LmUK

reverse them

    gADc
    wEDc
    ...
    KUmL

combine them back together

    gADcwEDcnAHIsRWIX9mcvxCIlxGbgcCSKUmL

and ROT13 the string

    tNQpjRQpaNUVfEJVK9zpikPVykTotpPFXHzY

And there's your Stacky program.

Why? Because I want you to suffer ;)

-Braden
